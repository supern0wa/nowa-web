import Auth from './Auth';
import AuthLogin from './AuthLogin';
import AuthRegister from './AuthRegister';

export default [
	{
		path: '/auth',
		component: Auth,
		children: [
			{
				path: '/',
				redirect: 'login'
			},
			{
				path: 'login',
				component: AuthLogin
			},
			{
				path: 'register',
				component: AuthRegister
			}
		]
	},
]
