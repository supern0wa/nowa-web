import VueRouter from 'vue-router';
import Http from './http';

export default function install(Vue) {
	if (install.installed) {
		console.warn('Admin core is already installed.');
		return;
	}

	install.installed = true;

	Vue.use(VueRouter);
	Vue.use(Http);
}

