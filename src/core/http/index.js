import VueResource from 'vue-resource';

export default function install(Vue) {
	Vue.use(VueResource);

	// VueResource Request interceptors
	Vue.http.interceptors.push((request, next) => {
		next();
	});
}

export getHeaders from './headers';
