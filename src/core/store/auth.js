import Vue from 'vue';

export default {
	namespaced: true,
	state: {
		authenticated: false,
	},
	getters: {
		isAuthenticated(state, getters, rootState) {
			let expiresAt = JSON.parse(localStorage.getItem('expires_at'));

			return new Date().getTime() < expiresAt && state.authenticated;
		}
	},
	actions: {
		login({state, commit, rootState}, payload) {
			return Vue.http.post(`/oauth/token`,
				{
					grant_type: 'password',
					username: payload.email,
					password: payload.password,
					access_token: '5lGOggFQhMEMoEXHC5PeEntc6MsuuZm5'
				},
				{
					emulateJSON: true,
				}
			);
		},

		register({state, commit, rootState}, payload) {
			return Vue.http.post(`${process.env.AUTH_URL}/register`,
				{
					email: payload.email,
					password: payload.password,
				},
				{ emulateJSON: true }
			);
		},

		logout({state, commit, rootState}, payload) {
			localStorage.removeItem('access_token');
			localStorage.removeItem('id_token');
			localStorage.removeItem('expires_at');

			state.authenticated = false;
		},

		// TODO: We need to provide a way to the redirect uri be dynamic
		social({state, commit, rootState}, payload) {
			window.location.href = `${process.env.AUTH_URL}/auth/${payload.provider}`;
		},

		// Authorization code callback
		handle({state, commit, rootState}, payload) {
			return new Promise((resolve, reject) => {
				console.warning('we need to handle callback');
				resolve();
			});
		}
	},
	mutations: {
		setSession(state, payload) {
			localStorage.setItem('id_token', payload.idToken);

			state.authenticated = true;
		},
	}
}
