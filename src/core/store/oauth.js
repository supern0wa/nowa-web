import Vue from 'vue';

export default {
	namespaced: true,
	actions: {
		getToken({state, commit}, payload) {
			return Vue.http.post('/oauth/token', {})
				.then(response => {
					commit('setSession', response.body);
					payload.callback();
				}, err => console.error(err));
		}
	},
	mutations: {
		setSession(state, payload) {
			console.log(payload);
			let expires_at = payload.expires_in * 1000 + new Date().getTime();

			localStorage.setItem('access_token', payload.access_token);
			localStorage.setItem('expires_at', expires_at);
		}
	}
}
