import Vue from 'vue';
import Vuex from 'vuex';

import auth from './auth';
import oauth from './oauth';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth,
		oauth
	}
});
