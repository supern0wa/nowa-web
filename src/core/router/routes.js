import authRoutes from '../../components/auth/routes';

export default [
	...authRoutes,
	{ path: '*', redirect: 'auth' }
];
