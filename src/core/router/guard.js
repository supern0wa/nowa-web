import store from '../store';

module.exports = router => {
	router.beforeEach((to, from, next) => {
		var access_token = localStorage.getItem('access_token');

		// If there's no token, get a new one
		if (!access_token) {
			store.dispatch('oauth/getToken', {
				callback: () => next()
			});
		} else {
			var expires_at = localStorage.getItem('expires_at');

			// Token expired
			if (new Date(parseInt(expires_at)) > new Date()) {
				// FIXME: Use refresh token
				store.dispatch('oauth/getToken', {
					callback: () => next()
				});
			} else {
				next();
			}
		}

		// TODO: We need to handle private area yet
		/* Route requires authentication
		if (to.matched.some(record => record.meta.requiresAuth)) {
			// User is already logged in
			if (store.getters['auth/isAuthenticated']) {
				let token = JSON.parse(localStorage.getItem('token'));

				// Token has expired
				if (new Date(token.expire_date) < new Date()) {
					// Refresh access token
					store.dispatch('oauth/refreshGrant', token)
						.then(response => {
							// Set new access token then continue
							store.commit('oauth/setToken', response.data);

							next();
						}, response => {
							// Handle error then redirect to auth
							store.commit('mushi/logger/handleError', response);
							next({ path: '/home' })
						});
				} else {
					// Token is valid then continue
					next();
				}
			} else {
				// User is not logged in then redirect to auth
				next({ path: '/home' });
			}
		} else {
			// Route is public then continue
			next();
		}*/
	});
}
