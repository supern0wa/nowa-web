import Vue from 'vue';
import App from './App';
import { core, store, router } from './core';

Vue.use(core);

new Vue({
	el: '#app',
	router,
	store,
	render: createElement => createElement(App)
});
