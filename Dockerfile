FROM node:alpine

# Create app directory
WORKDIR /usr/app

# Copy package.json
COPY package.json .
COPY package-lock.json .

# Install dependencies
RUN npm install

# Bundle app source
COPY . .

# Expose port
ARG PORT=8080
ENV PORT ${PORT}
EXPOSE $PORT

CMD ["npm", "start"]
