var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var webpack = require('webpack');
var config = require('./webpack.config');
var history = require('connect-history-api-fallback');
var devMiddleware = require('webpack-dev-middleware');
var hotMiddleware = require('webpack-hot-middleware');

const app = express();
const compiler = webpack(config);

const devMiddlewareInstance = devMiddleware(compiler, {
	noInfo: true,
	publicPath: '/',
	index: 'index.html'
});

const hotMiddlewareInstance = hotMiddleware(compiler, {});
const staticFileMiddleware = express.static(path.resolve(__dirname, 'public'))

app.use(bodyParser.urlencoded({ extended: true }));

// TODO: Should we move this to somewhere else?
app.post('/oauth/token', (req, res, next) => {
	var options = {
		method: 'POST',
		url: `${process.env.AUTH_URL}/login`,
		proxy: 'https://kong:8443',
		tunnel: false,
		json: true,
		form: {
			grant_type: req.body.grant_type,
			client_id: process.env.CLIENT_ID,
			client_secret: process.env.CLIENT_SECRET,
		}
	};

	if (options.form.grant_type == 'password') {
		options.form.username = req.body.username;
		options.form.password = req.body.password;
		options.headers = {
			'Authorization': `Bearer ${req.body.access_token}`
		}
	}

	return request(options, (err, _, body) => {
		if (err)
			next(err);

		return res.json(body);
	});
});

// Error middlware
app.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.send(err.message);

	console.log(err.stack);
});

app.use(history());
app.use(devMiddlewareInstance);
app.use(hotMiddlewareInstance);
app.use('/', staticFileMiddleware);

devMiddlewareInstance.waitUntilValid(() => {
	let uri = 'http://localhost:' + (process.env.PORT || 8080);

	console.log(`> Listening at ${uri} \n`);
});

module.exports = app.listen(process.env.PORT || 8080, '0.0.0.0', (error) => {
	if(error)
		return console.log(error);
});
