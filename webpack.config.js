var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'cheap-module-eval-source-map',
	entry: {
		'main': [
			'webpack-hot-middleware/client',
			'./src/main.js'
		],
	},
	output: {
		publicPath: '/'
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {}
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				exclude: '/node_modules'
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules',
				include: [
					path.resolve(__dirname, 'src')
				]
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
		]
	},
	plugins: [
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'development',
			CLIENT_ID: null,
			AUTH_URL: null,
			AUTH_CALLBACK: null
		}),
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/index.html',
			title: 'Nova Web',
			inject: true,
			chunks: ['main']
		})
	]
};
